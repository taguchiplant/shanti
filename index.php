<?php
session_start();

require_once("config/config.php");
require_once("model/Product.php");

if(!isset($_SESSION['User'])){
  header('Location: login.php');
  exit;
}


//接続
try {
 $product = new Product($host,$dbname,$user,$pass);
 $product->connectDB();

 if (isset($_GET['find'])) {
 //参照処理（条件付き）
  $rs_product['Product'] = $product->findById($_GET['find']);
}

  //参照処理
  $result = $product->findAll();






}
 catch (PDOException $e) {
 print 'エラー'.$e->getMessage();
 }

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>サンプル_植物と雑貨のセレクトショップ・石垣島・シャンティ・ガーデン</title>
  <link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body>

  <div id="header" class="show" style="display: block;">
    <div id="title">
  <p><a href='index.php' >【サンプル】植物と雑貨のセレクトショップ・石垣島・シャンティ・ガーデン</a></p>
</div>
<div id="top_menu">
  <ul>
    <li><a href='index.php' ><img src="img/base/header_inquire.gif" alt="1"></a></li>
    <li><a href='index.php' ><img src="img/base/header_myaccount.gif" alt="1"></a></li>
    <li><a href='index.php' ><img src="img/base/header_home.gif" alt="1"></a></li>
    <li><a href='admin.php' ><p><?php if ($_SESSION['User']['role'] == 0) {echo "管理者画面";} ?></p></a></li>
    <li><a href='login.php' ><p>ログイン</p></a></li>
  </ul>
</div>
  </div>

  <div id="wrapper">
    <div id="sub">
      <table>
        <tr>
          <th id="cate"><p>カテゴリー</p></th>
        </tr>
        <tr>
          <td><a href='index.php' >種</a></td>
        </tr>
        <tr>
          <td><a href='index.php' >苗</a></td>
        </tr>
        <tr>
          <td><a href='index.php' >フルーツ・野菜・生ハーブ<br>（生産者直送）</a></td>
        </tr>
        <tr>
          <td><a href='index.php' >石垣発・オリジナル手作り雑貨</a></td>
        </tr>
      </table>
      <div id="cart">
      <a href='cart.php' ><img src="img/base/side_cart.jpg" alt="カートを見る"></a>
    </div>

    <table>
          <tr>
        <th id="cate"><p>商品一覧</p></th>
      </tr>
        <?php foreach ($result as $row):?>
      <tr>
        <td><a href="?find=<?=$row["product_number"]?>"><?=$row["product_name"]?></a></td>
      </tr>
      <?php endforeach;?>
    </table>



    </div>

    <div id="main" >
      <div <?php if (!isset($_GET['find'])) {echo "style='display: none;'";} ?>>
      <div id="main_menu">
        <p><a href='index.php'>ホーム</a> ＞ <a href='index.php'>苗</a> ＞ <a href='index.php'>チョコレートの原料植物　カカオ　苗</a></p>
      </div>
      <div id="main_title">
        <p><?=$rs_product['Product']['product_name']?></p>
      </div>
      <div id="main_img">
        <img src="<?=$rs_product['Product']['main_img']?>" alt="メイン" >
      </div>
      <div id="description">
        <p><?=$rs_product['Product']['description']?></p>
      </div>


    <div id="sub_img">
      <img src="<?=$rs_product['Product']['sub_img1']?>" alt="サブ１" >
    </div>
    <div id="sub_img">
      <img src="<?=$rs_product['Product']['sub_img2']?>" alt="サブ２" >
    </div>

    <div id="link">
    <p><a href='index.php'>買い物を続ける</a></p>
    <p><a href='index.php'>この商品について問い合わせる</a></p>
    <p><a href='index.php'>この商品を友達に教える</a></p>
  </div>

  <div id="purchase">
    <!-- フォーム -->
    <form name="form" action="cart.php" method="POST">
    <table>
      <tr>
        <th><p>・ 販売価格</p></th>
        <td><p><?=number_format($rs_product['Product']['price'])?>円(税込<?=number_format($rs_product['Product']['price']*1.08)?>円)</p></td>
      </tr>
      <tr>
        <th><p>・ 購入数</p></th>
          <input type="hidden" name="product_name" value="<?=$rs_product['Product']['product_name']?>">
          <input type="hidden" name="price" value="<?=$rs_product['Product']['price']?>">

            <input type="hidden" name="product_number" value="<?=$rs_product['Product']['product_number']?>">
          <td><input type="number" value="1" min="1" name="quantity" ></td>
      </tr>
    </table>


  <div id="send">
    <input type="image"  name="submit" src="img/base/detail_cart.jpg" alt="カートへ入れる">
    <p><a href='index.php'>» 特定商取引法に基づく表記 (返品など)</a></p>
  </div>
</form>
<!-- フォームここまで -->
  </div>
</div>


  <div id="home_main" <?php if (isset($_GET['find'])) {echo "style='display: none;'";} ?>>
    <div id="main_title">
      <p>ホーム</p>
    </div>
    <div id="thumbnail_box">
          <?php foreach ($result as $row):?>
      <div id="thumbnail">
        <a href="?find=<?=$row["product_number"]?>"><img src="<?=$row["main_img"]?>" alt="サムネイル"></a>
        <a href="?find=<?=$row["product_number"]?>"><p><?=$row["product_name"]?></p></a>
        <p><?=number_format($row["price"])?>円(税込<?=number_format($row["price"]*1.08)?>円)</p>
      </div>
          <?php endforeach;?>
      </div>


    </div>


    </div>


  </div>


  </div>


  <div id="footer">
<p>当店の商品の写真、記事などの記載内容は、すべて石垣島・シャンティ・ガーデンに帰属します。（無断転載厳禁）</p>
<p>Copyright(C)2010 石垣島・シャンティ・ガーデン, ALL Rights Reserved.</p>
        </div>

  </div>


</div>
</body>
</html>
