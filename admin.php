<?php
session_start();
echo "misaki_git";

require_once("config/config.php");
require_once("model/Product.php");

if(!isset($_SESSION['User'])){
  header('Location: login.php');
  exit;
}
if($_SESSION['User']['role'] != 0){
  header('Location: login.php');
  exit;
}

//接続
try {
 $admin_product = new Product($host,$dbname,$user,$pass);
 $pdo = $admin_product->connectDB();


 if ($_POST) {

//トランザクションで登録処理
$pdo->beginTransaction();
try {
   $admin_product->fileCopy($_POST, $_FILES);
   $admin_product->add($_POST, $_FILES);
   print_r($_FILES);
   $pdo->commit();
 } catch (PDOException $e) {
    $pdo->rollBack();
  }
}

$result =  $admin_product->findAll();


}
 catch (PDOException $e) {
 print 'エラー'.$e->getMessage();
 }


?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>サンプル_管理画面シャンティ・ガーデン</title>
  <link rel="stylesheet" type="text/css" href="css/admin.css">

</head>
<body>
  <div id="wrapper">

  <div id="header" class="show" style="display: block;">
  <a href='index.php' >【サンプル】管理画面シャンティ・ガーデン</a></p>
  </div>

  <form name="form" action="admin.php" method="POST" enctype="multipart/form-data">
    <table id="form_table">
    <tr>
      <th>商品名:</th>
      <td><input type="text" name="product_name" value=""></td>
        <tr>
      <th>商品番号:</th>
      <td><input type="text" name="product_number" value=""></td>
    </tr>
    <tr>
      <th>価格:</th>
      <td><input type="text" name="price" value=""></td>
    </tr>
    <tr>
      <th>在庫数:</th>
      <td><input type="text" name="stock" value=""></td>
    </tr>
    <tr>
      <th>説明:</th>
      <td><textarea type="text" name="description" value=""></textarea></td>
    </tr>
    <tr>
      <th>メイン写真:</th>
      <td><input type="file" name="main_img" value=""></td>
    </tr>
    <tr>
      <th>サブ写真1:</th>
      <td><input type="file" name="sub_img1" value=""></td>
    </tr>

    <tr>
      <th>サブ写真2:</th>
      <td><input type="file" name="sub_img2" value=""></td>
    </tr>

      <input type="hidden" name="id" value="">
    </table>
 <div id="crad">
　<input type="submit" name="" value="登録">
 </div>
</form>

<table id="product_all">
    <p>登録商品一覧</p>
    <tr>
      <th>商品番号</th>
      <td>商品名</td>
      <td>価格</td>
      <td>在庫</td>
    </tr>
    <?php foreach ($result as $row):?>
  <tr>
    <th><?=$row["product_number"]?></th>
    <td><?=$row["product_name"]?></td>
    <td><?=$row["price"]?></td>
    <td><?=$row["stock"]?></td>
  </tr>
  <?php endforeach;?>
</table>

  </div>



  <div id="footer">
<p>当店の商品の写真、記事などの記載内容は、すべて石垣島・シャンティ・ガーデンに帰属します。（無断転載厳禁）</p>
<p>Copyright(C)2010 石垣島・シャンティ・ガーデン, ALL Rights Reserved.</p>
        </div>





</body>
</html>
