<?php

require_once("config/config.php");
require_once("model/Cart.php");

//接続
try {
 $cart = new Cart($host,$dbname,$user,$pass);
 $cart->connectDB();

//削除
 if (isset($_GET['del'])) {
   $cart->delete($_GET['del']);
 }

 //編集
  if (isset($_GET['edit'])) {
    $cart->edit($_GET);
  }

//追加
if ($_POST) {
  $cart->add($_POST);
}

//参照
$result = $cart->findAll();

}
 catch (PDOException $e) {
 print 'エラー'.$e->getMessage();
 }



?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>サンプル_植物と雑貨のセレクトショップ・石垣島・シャンティ・ガーデン</title>
  <link rel="stylesheet" type="text/css" href="css/cart.css">

</head>
<body>
  <div id="wrapper">

  <div id="header" class="show" style="display: block;">
  <a href='index.php' >【サンプル】植物と雑貨のセレクトショップ・石垣島・シャンティ・ガーデン</a></p>
  </div>
<form name="form" action="cart.php" method="GET">
  <table>
    <tr>
      <th width="300">
        <p>商品名</p>
      </th>
      <th width="150">
        <p>販売価格</p>
      </th>
      <th width="65">
        <p>数量</p>
      </th>
      <th width="50">
        <p>変更</p>
      </th>
      <th width="150">
        <p>小計</p>
      </th>
    </tr>
<?php $total = 0 ?>
<?php foreach ($result as $row):?>
    <tr>
      <td>
        <p><?=$row["product_name"]?></p>
      </td>
      <input type="hidden" name="id" value="<?=$row["id"]?>">
      <td>
        <p><?= number_format($row["price"])."円(税込み".number_format($row["price"] * 1.08 ). "円)"?></p>
      </td>
      <td>
        <p><input type="text" value="<?=$row["quantity"]?>" name="quantity" ></p>
      </td>
      <td>
        <p><a href="?del=<?=$row["id"]?>" onclick="if(!confirm('ID<?=$row["id"]?>を削除しますか？')) return false;">削除</a>
      </td>
      <td>
        <p><?= number_format($row["price"] * $row["quantity"] *1.08)."円"; ?></p>
      </td>
    </tr>
<?php $total += $row["price"] * $row["quantity"]?>
<?php endforeach;?>
<tr>
  <th>
  </th>
  <th>
</th>
<th>
  <p>商品合計</p>
</th>
<th>
</th>
<th>
<p><?=number_format($total*1.08)."円" ?></p>
</th>
</tr>

  </table>
<div id="crad">
　<input type="submit" name="edit" value="変更">
　<input type="submit" name="submit" value="カートを空にする">
</div>
</form>

<div id="box">
<div id="continue">
<p>買い物を続ける</p>
</div>

<div id="move">
  <p>レジへ進む</p>
</div>
</div>


  </div>

  <div id="footer">
<p>当店の商品の写真、記事などの記載内容は、すべて石垣島・シャンティ・ガーデンに帰属します。（無断転載厳禁）</p>
<p>Copyright(C)2010 石垣島・シャンティ・ガーデン, ALL Rights Reserved.</p>
        </div>





</body>
</html>
