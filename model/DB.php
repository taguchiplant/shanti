<?php
//プロパティ
class DB{

private $host;
private $dbname;
private $user;
private $pass;
protected $con;


//コンストラクタ
function __construct($host,$dbname,$user,$pass){
$this->host = $host;
$this->dbname = $dbname;
$this->user = $user;
$this->pass = $pass;
}

//メソッド
public function connectDB(){
 $this->con = new PDO('mysql:host='.$this->host.';dbname='.$this->dbname,$this->user,$this->pass);
 if(!$this->con){
   echo "DB接続できませんでした。";
   die();
 }
 return $this->con;
}




}

?>
