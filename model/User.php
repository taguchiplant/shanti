<?php
require_once("DB.php");

class User extends DB {

// 参照（全て）
public function findAll(){
  $sql = "SELECT * FROM user";
    $stmt = $this->con->prepare($sql);
    $params = array();
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    return $result;
}

//ログイン
public function login($arr){
  $sql =  "SELECT * FROM user WHERE email = :email AND password = :password";
  $stmt = $this->con->prepare($sql);
  $params = array(':email'=>$arr['email'], ':password'=>$arr['password']);
  $stmt->execute($params);
  // $result = $stmt->rowCount();
  $result = $stmt->fetch();
  return $result;
}


}
?>
