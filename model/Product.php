<?php
require_once("DB.php");

class Product extends DB {

// 参照（全て）
public function findAll(){
  $sql = "SELECT * FROM product";
    $stmt = $this->con->prepare($sql);
    $params = array();
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    return $result;
}


//参照（条件付き）
public function findById($product_number){
  $sql =  "SELECT * FROM product WHERE product_number = :product_number";
  $stmt = $this->con->prepare($sql);
  $params = array(':product_number'=>$product_number);
  $stmt->execute($params);
  $result = $stmt->fetch();
  return $result;
}


//商品登録
public function add($arr, $files){
  // print_r($arr);
$sql ="INSERT INTO product(product_number, product_name, price, description, stock, main_img, sub_img1, sub_img2, create_date) VALUES(:product_number, :product_name, :price, :description, :stock, :main_img, :sub_img1, :sub_img2, :create_date)";
$stmt = $this->con->prepare($sql);
$params = array(
  ':product_number' => $arr['product_number'],
  ':product_name'=>$arr['product_name'] ,
  ':price'=>$arr['price'] ,
  ':description'=>$arr['description'] ,
  ':stock'=>$arr['stock'] ,
  ':main_img'=>"img/product/".$arr['product_number']."/".$files['main_img']['name'],
  ':sub_img1'=>"img/product/".$arr['product_number']."/".$files['sub_img1']['name'] ,
  ':sub_img2'=>"img/product/".$arr['product_number']."/".$files['sub_img2']['name'] ,
  ':create_date'=>date('Y-m-d H:i:s')
);
return $stmt->execute($params);
}

//登録画像ファイルのコピー
public function fileCopy($arr, $files){
if(!file_exists("img/product/".$arr['product_number'])){
mkdir("img/product/".$arr['product_number']);
}
if(!empty($files['main_img']['tmp_name'])){
copy($files['main_img']['tmp_name'],"img/product/".$arr['product_number']."/".$files['main_img']['name']);
}
if(!empty($files['sub_img1']['tmp_name'])){
copy($files['sub_img1']['tmp_name'],"img/product/".$arr['product_number']."/".$files['sub_img1']['name']);
}
if(!empty($files['sub_img2']['tmp_name'])){
copy($files['sub_img2']['tmp_name'],"img/product/".$arr['product_number']."/".$files['sub_img2']['name']);
}
}




}
?>
