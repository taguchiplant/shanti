<?php
require_once("DB.php");

class Cart extends DB {

// 参照（全て）
public function findAll(){
  $sql = "SELECT * FROM cart";
    $stmt = $this->con->prepare($sql);
    $params = array();
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    return $result;
}

//登録
public function add($arr){
  print_r($arr);
$sql ="INSERT INTO cart(user_id, product_number, product_name, price, quantity, create_date) VALUES(:user_id, :product_number, :product_name, :price, :quantity, :create_date)";
$stmt = $this->con->prepare($sql);
$params = array(
  ':user_id' => 1,
  ':product_number' => $arr['product_number'],
  ':product_name'=>$arr['product_name'],
  ':price'=>$arr['price'] ,
  ':quantity'=>$arr['quantity'] ,
  ':create_date'=>date('Y-m-d H:i:s')
  );
$stmt->execute($params);
}

//削除
public function delete($id= null){
 if(isset($id)){
   $sql = "DELETE FROM cart WHERE id=:id";
   $params = array(':id'=>$id);
   $stmt = $this->con->prepare($sql);
   $stmt->execute($params);
 }
}

// 編集処理
public function edit($arr){
$sql ="UPDATE cart SET quantity = :quantity WHERE id = :id";
$stmt = $this->con->prepare($sql);
$params = array(
  ':id' => $arr['id'],
  ':quantity'=>$arr['quantity']
  );
$stmt->execute($params);
}


//
// public function edit($arr){
// for ($i = 1; $i <= 10; $i++) {
//  ${"v".$i."001"}=[n行1マス目];
//  ${"v".$i."002"}=[n行2マス目];
//  $sql = "UPDATE cart SET $NAME1 = ${"v".$i."001"}, $NAME2 = ${"v".$i."001"} WHERE ID = $ID01";
//  $stmt = $dbh -> prepare( $sql );
//  $stmt -> execute();
// }
// }



}
?>
